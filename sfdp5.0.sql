/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : sfdp5.0

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-03-23 23:45:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wf_goods
-- ----------------------------
DROP TABLE IF EXISTS `wf_goods`;
CREATE TABLE `wf_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '采购标题',
  `type` int(11) DEFAULT NULL COMMENT '采购类别',
  `miaoshu` varchar(255) DEFAULT NULL COMMENT '需求描述',
  `liyou` longtext COMMENT '采购原因',
  `sytime` datetime DEFAULT NULL COMMENT '使用日期',
  `cgmoney` varchar(255) DEFAULT NULL COMMENT '采购总额',
  `uid` int(10) DEFAULT '0' COMMENT '用户id',
  `status` int(10) DEFAULT '0' COMMENT '审核状态',
  `create_time` int(10) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(10) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='物资采购表';

-- ----------------------------
-- Records of wf_goods
-- ----------------------------

-- ----------------------------
-- Table structure for wf_goods_d1
-- ----------------------------
DROP TABLE IF EXISTS `wf_goods_d1`;
CREATE TABLE `wf_goods_d1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `d_id` int(10) DEFAULT '0' COMMENT '关联主表id',
  `c_title` varchar(255) DEFAULT NULL COMMENT '物品名称',
  `c_num` int(11) DEFAULT NULL COMMENT '采购数量',
  `c_dj` varchar(255) DEFAULT NULL COMMENT '采购单价',
  `c_con` varchar(255) DEFAULT NULL COMMENT '采购备注',
  `uid` int(10) DEFAULT '0' COMMENT '用户id',
  `status` int(10) DEFAULT '0' COMMENT '审核状态',
  `create_time` int(10) DEFAULT '0' COMMENT '新增时间',
  `update_time` int(10) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='goods子表';

-- ----------------------------
-- Records of wf_goods_d1
-- ----------------------------

-- ----------------------------
-- Table structure for wf_sfdp_btable
-- ----------------------------
DROP TABLE IF EXISTS `wf_sfdp_btable`;
CREATE TABLE `wf_sfdp_btable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '表名称',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '表别名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of wf_sfdp_btable
-- ----------------------------

-- ----------------------------
-- Table structure for wf_sfdp_design
-- ----------------------------
DROP TABLE IF EXISTS `wf_sfdp_design`;
CREATE TABLE `wf_sfdp_design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_bill` varchar(255) DEFAULT NULL COMMENT '表单名称',
  `s_title` varchar(255) DEFAULT NULL COMMENT '表名',
  `s_db` varchar(255) DEFAULT NULL COMMENT '数据库表名',
  `s_search` longtext,
  `s_list` longtext,
  `s_design` int(1) unsigned zerofill NOT NULL DEFAULT '0' COMMENT '设计状态0：设计中|1：开始设计|2：启用部署',
  `s_look` int(1) unsigned zerofill NOT NULL DEFAULT '0' COMMENT '是否锁定',
  `s_field` longtext,
  `add_user` varchar(255) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `s_db_bak` int(1) NOT NULL DEFAULT '0' COMMENT '0：不存在备份表 1：存在',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_sfdp_design
-- ----------------------------
INSERT INTO `wf_sfdp_design` VALUES ('1', 'D202103239778', '物资采购表', 'goods', '[]', '[]', '2', '1', '{\"name\":\"物资采购表\",\"name_db\":\"goods\",\"tpfd_id\":\"SFDP0540121\",\"tpfd_btn\":[\"add\",\"Edit\",\"Del\",\"View\"],\"tpfd_script\":\"\",\"tpfd_class\":\"\",\"list\":{\"Id221153245\":{\"tr\":\"Id221153245\",\"data\":{\"text_1159605\":{\"tpfd_id\":\"text_1159605\",\"tr_id\":\"Id221153245\",\"tpfd_name\":\"采购标题\",\"tpfd_db\":\"title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"}},\"type\":\"1\"},\"Id221253268\":{\"tr\":\"Id221253268\",\"data\":{\"dropdown_1300239\":{\"tpfd_id\":\"dropdown_1300239\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"采购类别\",\"tpfd_db\":\"type\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"0\",\"tpfd_data\":[\"办公物品\",\"生活用品\",\"其他\"],\"tpfd_check\":\"1\",\"checkboxes_func\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"dropdown\"},\"text_1402234\":{\"tpfd_id\":\"text_1402234\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"需求描述\",\"tpfd_db\":\"miaoshu\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2},\"Id221431335\":{\"tr\":\"Id221431335\",\"data\":{\"textarea_1438656\":{\"tpfd_id\":\"textarea_1438656\",\"tr_id\":\"Id221431335\",\"tpfd_name\":\"采购原因\",\"tpfd_db\":\"liyou\",\"tpfd_dblx\":\"longtext\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"textarea\"}},\"type\":\"1\"},\"Id221506935\":{\"tr\":\"Id221506935\",\"data\":{\"date_1511686\":{\"tpfd_id\":\"date_1511686\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"使用日期\",\"tpfd_db\":\"sytime\",\"tpfd_dblx\":\"datetime\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"2\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"date\"},\"text_1601315\":{\"tpfd_id\":\"text_1601315\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"采购总额\",\"tpfd_db\":\"cgmoney\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"0\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2}},\"sublist\":{\"Id221628720\":{\"id\":\"Id221628720\",\"title\":\"采购一览表\",\"data\":{\"text_1651839\":{\"tpfd_id\":\"text_1651839\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"物品名称\",\"tpfd_db\":\"c_title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"},\"number_173613\":{\"tpfd_id\":\"number_173613\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购数量\",\"tpfd_db\":\"c_num\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"number\"},\"text_1757965\":{\"tpfd_id\":\"text_1757965\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购单价\",\"tpfd_db\":\"c_dj\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"3\",\"td_type\":\"text\"},\"text_1823502\":{\"tpfd_id\":\"text_1823502\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购备注\",\"tpfd_db\":\"c_con\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"4\",\"td_type\":\"text\"}},\"type\":\"4\"}},\"tpfd_time\":\"2021-03-23 22:05:40\",\"tpfd_ver\":\"v5.0\"}', 'Sys', '1616508320', '0');

-- ----------------------------
-- Table structure for wf_sfdp_design_ver
-- ----------------------------
DROP TABLE IF EXISTS `wf_sfdp_design_ver`;
CREATE TABLE `wf_sfdp_design_ver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) DEFAULT NULL COMMENT '关联ID',
  `s_bill` varchar(255) DEFAULT NULL COMMENT '业务编号',
  `s_name` varchar(255) NOT NULL COMMENT '业务名称',
  `s_db` varchar(255) NOT NULL COMMENT '数据表名',
  `s_list` longtext COMMENT '列表字段',
  `s_search` longtext COMMENT '查询字段',
  `s_fun_id` int(11) DEFAULT NULL,
  `s_fun_ver` longtext COMMENT '脚本版本',
  `s_field` longtext COMMENT '字段JSON',
  `add_user` int(2) DEFAULT NULL COMMENT '用户id',
  `add_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `status` int(1) unsigned zerofill NOT NULL DEFAULT '0' COMMENT '版本状态0:停用|1:启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_sfdp_design_ver
-- ----------------------------
INSERT INTO `wf_sfdp_design_ver` VALUES ('1', '1', 'D202103238141', '物资采购表', 'goods', '[]', '[]', null, '', '{\"name\":\"物资采购表\",\"name_db\":\"goods\",\"tpfd_id\":\"SFDP0540121\",\"tpfd_btn\":[\"add\",\"Edit\",\"Del\",\"View\"],\"tpfd_script\":\"\",\"tpfd_class\":\"\",\"list\":{\"Id221153245\":{\"tr\":\"Id221153245\",\"data\":{\"text_1159605\":{\"tpfd_id\":\"text_1159605\",\"tr_id\":\"Id221153245\",\"tpfd_name\":\"采购标题\",\"tpfd_db\":\"title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"}},\"type\":\"1\"},\"Id221253268\":{\"tr\":\"Id221253268\",\"data\":{\"dropdown_1300239\":{\"tpfd_id\":\"dropdown_1300239\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"采购类别\",\"tpfd_db\":\"type\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"0\",\"tpfd_data\":[\"办公物品\",\"生活用品\",\"其他\"],\"tpfd_check\":\"1\",\"checkboxes_func\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"dropdown\"},\"text_1402234\":{\"tpfd_id\":\"text_1402234\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"需求描述\",\"tpfd_db\":\"miaoshu\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2},\"Id221431335\":{\"tr\":\"Id221431335\",\"data\":{\"textarea_1438656\":{\"tpfd_id\":\"textarea_1438656\",\"tr_id\":\"Id221431335\",\"tpfd_name\":\"采购原因\",\"tpfd_db\":\"liyou\",\"tpfd_dblx\":\"longtext\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"textarea\"}},\"type\":\"1\"},\"Id221506935\":{\"tr\":\"Id221506935\",\"data\":{\"date_1511686\":{\"tpfd_id\":\"date_1511686\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"使用日期\",\"tpfd_db\":\"sytime\",\"tpfd_dblx\":\"datetime\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"2\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"date\"},\"text_1601315\":{\"tpfd_id\":\"text_1601315\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"采购总额\",\"tpfd_db\":\"cgmoney\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"0\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2}},\"sublist\":{\"Id221628720\":{\"id\":\"Id221628720\",\"title\":\"采购一览表\",\"data\":{\"text_1651839\":{\"tpfd_id\":\"text_1651839\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"物品名称\",\"tpfd_db\":\"c_title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"},\"number_173613\":{\"tpfd_id\":\"number_173613\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购数量\",\"tpfd_db\":\"c_num\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"number\"},\"text_1757965\":{\"tpfd_id\":\"text_1757965\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购单价\",\"tpfd_db\":\"c_dj\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"3\",\"td_type\":\"text\"},\"text_1823502\":{\"tpfd_id\":\"text_1823502\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购备注\",\"tpfd_db\":\"c_con\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"4\",\"td_type\":\"text\"}},\"type\":\"4\"}},\"tpfd_time\":\"2021-03-23 22:05:40\",\"tpfd_ver\":\"v5.0\"}', '1', '1616509133', '0');
INSERT INTO `wf_sfdp_design_ver` VALUES ('2', '1', 'D202103235509', '物资采购表', 'goods', '[]', '[]', null, '', '{\"name\":\"物资采购表\",\"name_db\":\"goods\",\"tpfd_id\":\"SFDP0540121\",\"tpfd_btn\":[\"add\",\"Edit\",\"Del\",\"View\"],\"tpfd_script\":\"\",\"tpfd_class\":\"\",\"list\":{\"Id221153245\":{\"tr\":\"Id221153245\",\"data\":{\"text_1159605\":{\"tpfd_id\":\"text_1159605\",\"tr_id\":\"Id221153245\",\"tpfd_name\":\"采购标题\",\"tpfd_db\":\"title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"}},\"type\":\"1\"},\"Id221253268\":{\"tr\":\"Id221253268\",\"data\":{\"dropdown_1300239\":{\"tpfd_id\":\"dropdown_1300239\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"采购类别\",\"tpfd_db\":\"type\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"0\",\"tpfd_data\":[\"办公物品\",\"生活用品\",\"其他\"],\"tpfd_check\":\"1\",\"checkboxes_func\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"dropdown\"},\"text_1402234\":{\"tpfd_id\":\"text_1402234\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"需求描述\",\"tpfd_db\":\"miaoshu\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2},\"Id221431335\":{\"tr\":\"Id221431335\",\"data\":{\"textarea_1438656\":{\"tpfd_id\":\"textarea_1438656\",\"tr_id\":\"Id221431335\",\"tpfd_name\":\"采购原因\",\"tpfd_db\":\"liyou\",\"tpfd_dblx\":\"longtext\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"textarea\"}},\"type\":\"1\"},\"Id221506935\":{\"tr\":\"Id221506935\",\"data\":{\"date_1511686\":{\"tpfd_id\":\"date_1511686\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"使用日期\",\"tpfd_db\":\"sytime\",\"tpfd_dblx\":\"datetime\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"2\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"date\"},\"text_1601315\":{\"tpfd_id\":\"text_1601315\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"采购总额\",\"tpfd_db\":\"cgmoney\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"0\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2}},\"sublist\":{\"Id221628720\":{\"id\":\"Id221628720\",\"title\":\"采购一览表\",\"data\":{\"text_1651839\":{\"tpfd_id\":\"text_1651839\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"物品名称\",\"tpfd_db\":\"c_title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"},\"number_173613\":{\"tpfd_id\":\"number_173613\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购数量\",\"tpfd_db\":\"c_num\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"number\"},\"text_1757965\":{\"tpfd_id\":\"text_1757965\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购单价\",\"tpfd_db\":\"c_dj\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"3\",\"td_type\":\"text\"},\"text_1823502\":{\"tpfd_id\":\"text_1823502\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购备注\",\"tpfd_db\":\"c_con\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"4\",\"td_type\":\"text\"}},\"type\":\"4\"}},\"tpfd_time\":\"2021-03-23 22:05:40\",\"tpfd_ver\":\"v5.0\"}', '1', '1616511218', '0');
INSERT INTO `wf_sfdp_design_ver` VALUES ('3', '1', 'D202103235415', '物资采购表', 'goods', '[]', '[]', null, '', '{\"name\":\"物资采购表\",\"name_db\":\"goods\",\"tpfd_id\":\"SFDP0540121\",\"tpfd_btn\":[\"add\",\"Edit\",\"Del\",\"View\"],\"tpfd_script\":\"\",\"tpfd_class\":\"\",\"list\":{\"Id221153245\":{\"tr\":\"Id221153245\",\"data\":{\"text_1159605\":{\"tpfd_id\":\"text_1159605\",\"tr_id\":\"Id221153245\",\"tpfd_name\":\"采购标题\",\"tpfd_db\":\"title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"}},\"type\":\"1\"},\"Id221253268\":{\"tr\":\"Id221253268\",\"data\":{\"dropdown_1300239\":{\"tpfd_id\":\"dropdown_1300239\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"采购类别\",\"tpfd_db\":\"type\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"0\",\"tpfd_data\":[\"办公物品\",\"生活用品\",\"其他\"],\"tpfd_check\":\"1\",\"checkboxes_func\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"dropdown\"},\"text_1402234\":{\"tpfd_id\":\"text_1402234\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"需求描述\",\"tpfd_db\":\"miaoshu\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2},\"Id221431335\":{\"tr\":\"Id221431335\",\"data\":{\"textarea_1438656\":{\"tpfd_id\":\"textarea_1438656\",\"tr_id\":\"Id221431335\",\"tpfd_name\":\"采购原因\",\"tpfd_db\":\"liyou\",\"tpfd_dblx\":\"longtext\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"textarea\"}},\"type\":\"1\"},\"Id221506935\":{\"tr\":\"Id221506935\",\"data\":{\"date_1511686\":{\"tpfd_id\":\"date_1511686\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"使用日期\",\"tpfd_db\":\"sytime\",\"tpfd_dblx\":\"datetime\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"2\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"date\"},\"text_1601315\":{\"tpfd_id\":\"text_1601315\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"采购总额\",\"tpfd_db\":\"cgmoney\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"0\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2}},\"sublist\":{\"Id221628720\":{\"id\":\"Id221628720\",\"title\":\"采购一览表\",\"data\":{\"text_1651839\":{\"tpfd_id\":\"text_1651839\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"物品名称\",\"tpfd_db\":\"c_title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"},\"number_173613\":{\"tpfd_id\":\"number_173613\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购数量\",\"tpfd_db\":\"c_num\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"number\"},\"text_1757965\":{\"tpfd_id\":\"text_1757965\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购单价\",\"tpfd_db\":\"c_dj\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"3\",\"td_type\":\"text\"},\"text_1823502\":{\"tpfd_id\":\"text_1823502\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购备注\",\"tpfd_db\":\"c_con\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"4\",\"td_type\":\"text\"}},\"type\":\"4\"}},\"tpfd_time\":\"2021-03-23 22:05:40\",\"tpfd_ver\":\"v5.0\"}', '1', '1616511354', '0');
INSERT INTO `wf_sfdp_design_ver` VALUES ('4', '1', 'D202103232564', '物资采购表', 'goods', '[]', '[]', null, '', '{\"name\":\"物资采购表\",\"name_db\":\"goods\",\"tpfd_id\":\"SFDP0540121\",\"tpfd_btn\":[\"add\",\"Edit\",\"Del\",\"View\"],\"tpfd_script\":\"\",\"tpfd_class\":\"\",\"list\":{\"Id221153245\":{\"tr\":\"Id221153245\",\"data\":{\"text_1159605\":{\"tpfd_id\":\"text_1159605\",\"tr_id\":\"Id221153245\",\"tpfd_name\":\"采购标题\",\"tpfd_db\":\"title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"}},\"type\":\"1\"},\"Id221253268\":{\"tr\":\"Id221253268\",\"data\":{\"dropdown_1300239\":{\"tpfd_id\":\"dropdown_1300239\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"采购类别\",\"tpfd_db\":\"type\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"0\",\"tpfd_data\":[\"办公物品\",\"生活用品\",\"其他\"],\"tpfd_check\":\"1\",\"checkboxes_func\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"dropdown\"},\"text_1402234\":{\"tpfd_id\":\"text_1402234\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"需求描述\",\"tpfd_db\":\"miaoshu\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2},\"Id221431335\":{\"tr\":\"Id221431335\",\"data\":{\"textarea_1438656\":{\"tpfd_id\":\"textarea_1438656\",\"tr_id\":\"Id221431335\",\"tpfd_name\":\"采购原因\",\"tpfd_db\":\"liyou\",\"tpfd_dblx\":\"longtext\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"textarea\"}},\"type\":\"1\"},\"Id221506935\":{\"tr\":\"Id221506935\",\"data\":{\"date_1511686\":{\"tpfd_id\":\"date_1511686\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"使用日期\",\"tpfd_db\":\"sytime\",\"tpfd_dblx\":\"datetime\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"2\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"date\"},\"text_1601315\":{\"tpfd_id\":\"text_1601315\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"采购总额\",\"tpfd_db\":\"cgmoney\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"0\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2}},\"sublist\":{\"Id221628720\":{\"id\":\"Id221628720\",\"title\":\"采购一览表\",\"data\":{\"text_1651839\":{\"tpfd_id\":\"text_1651839\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"物品名称\",\"tpfd_db\":\"c_title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"},\"number_173613\":{\"tpfd_id\":\"number_173613\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购数量\",\"tpfd_db\":\"c_num\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"number\"},\"text_1757965\":{\"tpfd_id\":\"text_1757965\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购单价\",\"tpfd_db\":\"c_dj\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"3\",\"td_type\":\"text\"},\"text_1823502\":{\"tpfd_id\":\"text_1823502\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购备注\",\"tpfd_db\":\"c_con\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"4\",\"td_type\":\"text\"}},\"type\":\"4\"}},\"tpfd_time\":\"2021-03-23 22:05:40\",\"tpfd_ver\":\"v5.0\"}', '1', '1616511678', '0');
INSERT INTO `wf_sfdp_design_ver` VALUES ('5', '1', 'D202103234268', '物资采购表', 'goods', '[]', '[]', null, '', '{\"name\":\"物资采购表\",\"name_db\":\"goods\",\"tpfd_id\":\"SFDP0540121\",\"tpfd_btn\":[\"add\",\"Edit\",\"Del\",\"View\"],\"tpfd_script\":\"\",\"tpfd_class\":\"\",\"list\":{\"Id221153245\":{\"tr\":\"Id221153245\",\"data\":{\"text_1159605\":{\"tpfd_id\":\"text_1159605\",\"tr_id\":\"Id221153245\",\"tpfd_name\":\"采购标题\",\"tpfd_db\":\"title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"}},\"type\":\"1\"},\"Id221253268\":{\"tr\":\"Id221253268\",\"data\":{\"dropdown_1300239\":{\"tpfd_id\":\"dropdown_1300239\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"采购类别\",\"tpfd_db\":\"type\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"0\",\"tpfd_data\":[\"办公物品\",\"生活用品\",\"其他\"],\"tpfd_check\":\"1\",\"checkboxes_func\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"dropdown\"},\"text_1402234\":{\"tpfd_id\":\"text_1402234\",\"tr_id\":\"Id221253268\",\"tpfd_name\":\"需求描述\",\"tpfd_db\":\"miaoshu\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2},\"Id221431335\":{\"tr\":\"Id221431335\",\"data\":{\"textarea_1438656\":{\"tpfd_id\":\"textarea_1438656\",\"tr_id\":\"Id221431335\",\"tpfd_name\":\"采购原因\",\"tpfd_db\":\"liyou\",\"tpfd_dblx\":\"longtext\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"textarea\"}},\"type\":\"1\"},\"Id221506935\":{\"tr\":\"Id221506935\",\"data\":{\"date_1511686\":{\"tpfd_id\":\"date_1511686\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"使用日期\",\"tpfd_db\":\"sytime\",\"tpfd_dblx\":\"datetime\",\"tpfd_dbcd\":\"0\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"xx_type\":\"2\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"date\"},\"text_1601315\":{\"tpfd_id\":\"text_1601315\",\"tr_id\":\"Id221506935\",\"tpfd_name\":\"采购总额\",\"tpfd_db\":\"cgmoney\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"0\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"text\"}},\"type\":2}},\"sublist\":{\"Id221628720\":{\"id\":\"Id221628720\",\"title\":\"采购一览表\",\"data\":{\"text_1651839\":{\"tpfd_id\":\"text_1651839\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"物品名称\",\"tpfd_db\":\"c_title\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"1\",\"td_type\":\"text\"},\"number_173613\":{\"tpfd_id\":\"number_173613\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购数量\",\"tpfd_db\":\"c_num\",\"tpfd_dblx\":\"int\",\"tpfd_dbcd\":\"11\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"2\",\"td_type\":\"number\"},\"text_1757965\":{\"tpfd_id\":\"text_1757965\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购单价\",\"tpfd_db\":\"c_dj\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"3\",\"td_type\":\"text\"},\"text_1823502\":{\"tpfd_id\":\"text_1823502\",\"tr_id\":\"Id221628720\",\"tpfd_name\":\"采购备注\",\"tpfd_db\":\"c_con\",\"tpfd_dblx\":\"varchar\",\"tpfd_dbcd\":\"255\",\"tpfd_list\":\"no\",\"tpfd_chaxun\":\"no\",\"tpfd_zanwei\":\"\",\"tpfd_moren\":\"\",\"tpfd_read\":\"1\",\"tpfd_must\":\"0\",\"td\":\"4\",\"td_type\":\"text\"}},\"type\":\"4\"}},\"tpfd_time\":\"2021-03-23 22:05:40\",\"tpfd_ver\":\"v5.0\"}', '1', '1616511703', '1');

-- ----------------------------
-- Table structure for wf_sfdp_field
-- ----------------------------
DROP TABLE IF EXISTS `wf_sfdp_field`;
CREATE TABLE `wf_sfdp_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `sid` int(11) NOT NULL COMMENT '版本编号',
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字段名称',
  `name_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字段别名',
  `zanwei` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '占位内容',
  `moren` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '默认内容',
  `is_request` int(11) DEFAULT NULL COMMENT '是否必填',
  `is_read` int(11) DEFAULT NULL,
  `length` int(11) DEFAULT NULL COMMENT '长度',
  `type_lx` int(11) NOT NULL DEFAULT '0' COMMENT '选择类型',
  `type_data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字段格式内容',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字段类型',
  `data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字段数据',
  `function` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '调用的函数方法',
  `is_list` int(11) DEFAULT '0' COMMENT '是否列表',
  `is_search` int(11) DEFAULT '0' COMMENT '是否查询',
  `search_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '查询类型',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of wf_sfdp_field
-- ----------------------------
INSERT INTO `wf_sfdp_field` VALUES ('1', '1', 'id', 'int', '主键', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('2', '1', 'uid', 'int', '用户id', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('3', '1', 'status', 'int', '审核状态', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('4', '1', 'create_time', 'int', '新增时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('5', '1', 'update_time', 'int', '更新时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('6', '1', 'title', 'varchar', '采购标题', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '1', '1', 'like', '1616510011');
INSERT INTO `wf_sfdp_field` VALUES ('7', '1', 'type', 'int', '采购类别', '', '', '0', '1', '11', '0', '[\"\\u529e\\u516c\\u7269\\u54c1\",\"\\u751f\\u6d3b\\u7528\\u54c1\",\"\\u5176\\u4ed6\"]', 'dropdown', '0', '', '1', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('8', '1', 'miaoshu', 'varchar', '需求描述', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '0', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('9', '1', 'liyou', 'longtext', '采购原因', '', '', '0', '1', '0', '0', '\"\"', 'textarea', '', '', '0', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('10', '1', 'sytime', 'datetime', '使用日期', '', '', '0', '1', '0', '0', '\"\"', 'date', '2', '', '0', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('11', '1', 'cgmoney', 'varchar', '采购总额', '', '', '0', '0', '255', '0', '\"\"', 'text', '', '', '0', '0', '', '1616510010');
INSERT INTO `wf_sfdp_field` VALUES ('12', '2', 'id', 'int', '主键', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('13', '2', 'uid', 'int', '用户id', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('14', '2', 'status', 'int', '审核状态', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('15', '2', 'create_time', 'int', '新增时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('16', '2', 'update_time', 'int', '更新时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('17', '2', 'title', 'varchar', '采购标题', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('18', '2', 'type', 'int', '采购类别', '', '', '0', '1', '11', '0', '[\"\\u529e\\u516c\\u7269\\u54c1\",\"\\u751f\\u6d3b\\u7528\\u54c1\",\"\\u5176\\u4ed6\"]', 'dropdown', '0', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('19', '2', 'miaoshu', 'varchar', '需求描述', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('20', '2', 'liyou', 'longtext', '采购原因', '', '', '0', '1', '0', '0', '\"\"', 'textarea', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('21', '2', 'sytime', 'datetime', '使用日期', '', '', '0', '1', '0', '0', '\"\"', 'date', '2', '', '2', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('22', '2', 'cgmoney', 'varchar', '采购总额', '', '', '0', '0', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('23', '3', 'id', 'int', '主键', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('24', '3', 'uid', 'int', '用户id', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('25', '3', 'status', 'int', '审核状态', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('26', '3', 'create_time', 'int', '新增时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('27', '3', 'update_time', 'int', '更新时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('28', '3', 'title', 'varchar', '采购标题', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('29', '3', 'type', 'int', '采购类别', '', '', '0', '1', '11', '0', '[\"\\u529e\\u516c\\u7269\\u54c1\",\"\\u751f\\u6d3b\\u7528\\u54c1\",\"\\u5176\\u4ed6\"]', 'dropdown', '0', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('30', '3', 'miaoshu', 'varchar', '需求描述', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('31', '3', 'liyou', 'longtext', '采购原因', '', '', '0', '1', '0', '0', '\"\"', 'textarea', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('32', '3', 'sytime', 'datetime', '使用日期', '', '', '0', '1', '0', '0', '\"\"', 'date', '2', '', '2', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('33', '3', 'cgmoney', 'varchar', '采购总额', '', '', '0', '0', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('34', '4', 'id', 'int', '主键', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('35', '4', 'uid', 'int', '用户id', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('36', '4', 'status', 'int', '审核状态', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('37', '4', 'create_time', 'int', '新增时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('38', '4', 'update_time', 'int', '更新时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', null);
INSERT INTO `wf_sfdp_field` VALUES ('39', '4', 'title', 'varchar', '采购标题', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('40', '4', 'type', 'int', '采购类别', '', '', '0', '1', '11', '0', '[\"\\u529e\\u516c\\u7269\\u54c1\",\"\\u751f\\u6d3b\\u7528\\u54c1\",\"\\u5176\\u4ed6\"]', 'dropdown', '0', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('41', '4', 'miaoshu', 'varchar', '需求描述', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('42', '4', 'liyou', 'longtext', '采购原因', '', '', '0', '1', '0', '0', '\"\"', 'textarea', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('43', '4', 'sytime', 'datetime', '使用日期', '', '', '0', '1', '0', '0', '\"\"', 'date', '2', '', '2', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('44', '4', 'cgmoney', 'varchar', '采购总额', '', '', '0', '0', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', null);
INSERT INTO `wf_sfdp_field` VALUES ('45', '5', 'id', 'int', '主键', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('46', '5', 'uid', 'int', '用户id', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('47', '5', 'status', 'int', '审核状态', '', '', '0', '0', '11', '0', '', 'text', '', '', '0', '0', '', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('48', '5', 'create_time', 'int', '新增时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('49', '5', 'update_time', 'int', '更新时间', '', '', '0', '0', '11', '0', '', 'datetime', '', '', '0', '0', '', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('50', '5', 'title', 'varchar', '采购标题', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '1', '0', '0', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('51', '5', 'type', 'int', '采购类别', '', '', '0', '1', '11', '0', '[\"\\u529e\\u516c\\u7269\\u54c1\",\"\\u751f\\u6d3b\\u7528\\u54c1\",\"\\u5176\\u4ed6\"]', 'dropdown', '0', '', '0', '0', '0', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('52', '5', 'miaoshu', 'varchar', '需求描述', '', '', '0', '1', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('53', '5', 'liyou', 'longtext', '采购原因', '', '', '0', '1', '0', '0', '\"\"', 'textarea', '', '', '0', '0', '0', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('54', '5', 'sytime', 'datetime', '使用日期', '', '', '0', '1', '0', '0', '\"\"', 'date', '2', '', '0', '0', '0', '1616512262');
INSERT INTO `wf_sfdp_field` VALUES ('55', '5', 'cgmoney', 'varchar', '采购总额', '', '', '0', '0', '255', '0', '\"\"', 'text', '', '', '0', '0', '0', '1616512262');

-- ----------------------------
-- Table structure for wf_sfdp_function
-- ----------------------------
DROP TABLE IF EXISTS `wf_sfdp_function`;
CREATE TABLE `wf_sfdp_function` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fun_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `function` longtext COLLATE utf8_unicode_ci,
  `add_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0编辑中，1启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of wf_sfdp_function
-- ----------------------------

-- ----------------------------
-- Table structure for wf_sfdp_modue
-- ----------------------------
DROP TABLE IF EXISTS `wf_sfdp_modue`;
CREATE TABLE `wf_sfdp_modue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '标题',
  `dbtable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '数据表',
  `btn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '按钮',
  `script` int(11) DEFAULT NULL COMMENT '脚本',
  `field_name` longtext COLLATE utf8_unicode_ci,
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access` longtext COLLATE utf8_unicode_ci,
  `update_time` int(11) DEFAULT NULL,
  `order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of wf_sfdp_modue
-- ----------------------------
INSERT INTO `wf_sfdp_modue` VALUES ('1', '1', '物资采购表', 'goods', 'add,Edit,Del,View', null, '采购标题,采购类别', 'title,type', '[]', '1616509991', null);
INSERT INTO `wf_sfdp_modue` VALUES ('2', '2', '物资采购表', 'goods', 'add,Edit,Del,View', null, null, null, null, null, null);
INSERT INTO `wf_sfdp_modue` VALUES ('3', '3', '物资采购表', 'goods', 'add,Edit,Del,View', null, null, null, null, null, null);
INSERT INTO `wf_sfdp_modue` VALUES ('4', '4', '物资采购表', 'goods', 'add,Edit,Del,View', null, null, null, null, null, null);
INSERT INTO `wf_sfdp_modue` VALUES ('5', '5', '物资采购表', 'goods', 'add,Edit,Del,View', null, '采购标题', 'title', null, '1616512262', 'id desc');

-- ----------------------------
-- Table structure for wf_sfdp_script
-- ----------------------------
DROP TABLE IF EXISTS `wf_sfdp_script`;
CREATE TABLE `wf_sfdp_script` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) DEFAULT NULL COMMENT '关联ID',
  `s_bill` varchar(255) DEFAULT NULL COMMENT '脚本编号',
  `s_fun` longtext COMMENT '脚本代码',
  `add_user` varchar(255) DEFAULT NULL COMMENT '添加用户',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_sfdp_script
-- ----------------------------
