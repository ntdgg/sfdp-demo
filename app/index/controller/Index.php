<?php
// 本类由系统自动生成，仅供测试用途
namespace app\index\Controller;
use think\facade\Session;
use workflow\workflow;
use think\facade\View;
use think\facade\Db;
use think\facade\Request;


class Index {
	use \liliuwei\think\Jump; 
	
    public function index(){
		
		Session::set('uid', 1);
		Session::set('username', 'sys');
		Session::set('role', 1);
     return View::fetch();
    }
	public function welcome(){
	
      return  View::fetch();
    }
	
	
}