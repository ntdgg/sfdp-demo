<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::group('sfdp', function () {
	Route::rule('sfdpApi', '\sfdp\Api@sfdpApi');//业务流程设计API接口
	Route::rule('fApi', '\sfdp\Api@fApi');//业务脚本请求接口
	Route::rule('sfdpCurd', '\sfdp\Api@sfdpCurd');//业务脚本请求接口
	
	
});